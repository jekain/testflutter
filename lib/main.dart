import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Test',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Test'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final Random _random = Random();
  int _rColor = 0;
  int _gColor = 0;
  int _bColor = 0;
  bool _animated = false;
  bool _isAnimated = false;

  void _changeColor() {
    setState(() {
      _rColor = _random.nextInt(256);
      _gColor = _random.nextInt(256);
      _bColor = _random.nextInt(256);
      print('My Color r= ${_rColor} g= ${_gColor} b= ${_bColor}');
    });
  }

  void _startAnimation() {
    if (_isAnimated) return;
    Future.delayed(const Duration(milliseconds: 100), () {
      _animated = true;
      _isAnimated = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    _startAnimation();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Row(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Color.fromARGB(_rColor, _gColor, _bColor, 1),
              ),
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () => _changeColor(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    AnimatedDefaultTextStyle(
                      child: const Text('Hey there'),
                      style : _animated ? const TextStyle(
                        color: Colors.black,
                        fontSize: 30,
                      ) : const TextStyle(
                        color: Colors.white,
                        fontSize: 0,
                      ),
                      duration: const Duration(milliseconds: 1000),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      )
    );
  }
}
